import xlrd
import smtplib, ssl
import config
from datetime import datetime
from datetime import time



#Setup email
port = 587  # For starttls
smtp_server = "smtp.gmail.com"
sender_email = "canterboystech@gmail.com"
#receiver_email = "shaz.rule21@gmail.com"


#open the spreadsheet file
workbook =xlrd.open_workbook("C:/Users/w_sha/Desktop/RFID_Excel-master/PLX-DAQ-v2.11/PLX.xlsm")

#open a sheet thru index
worksheet = workbook.sheet_by_index(2)

#data
date_column = 1 #FIXED!
name_column = 2 #FIXED!
email_column = 3 #FIXED!

#date
system_date = datetime.today().date()

#index for date row
date_row = 0
excel_date = (worksheet.cell(date_row,date_column))


def used_row():
    empty_cell= False
    global row_index
    for row_index in range(0, total_rows):
        # set count empty cells
        count_empty = 0
        #print('Row: {}'.format(row_index))
        for col_index in range (0, total_cols):
            #get cell value
            cell_val = worksheet.cell(row_index, col_index).value
            if cell_val =='':
                empty_cell = True
                count_empty+= 1
            else:
                #set empty cell is false
                empty_cell = False

        if count_empty == total_cols:
            break

def send_email():
    context = ssl.create_default_context()
    with smtplib.SMTP(smtp_server, port) as server:
        server.ehlo()  # Can be omitted
        server.starttls(context=context)
        server.ehlo()  # Can be omitted
        server.login(sender_email, config.PASSWORD)
        server.sendmail(sender_email, receiver_email, message)
        print ("email sent successfully!")


#for date row, if date = today's date, go to same row + fixed email column,
#send email, date Row +1
try:


    #number of filled rows
    total_rows = worksheet.nrows
    total_cols = worksheet.ncols
    
    used_row()
    #print (row_index)
    #print(total_rows)
    threshold_time = time(21,00,0)#set threshold time

    for date_row in range (1, row_index):
        
        py_date = datetime(*xlrd.xldate_as_tuple((worksheet.cell(date_row,date_column)).value,workbook.datemode)).date()
        
                                                
        #print ((worksheet.cell(date_row,0)).value)
        #print (py_date)
        #print (py_time)
        #print(system_date)
        
        
        if (py_date == system_date):

            
            py_time = datetime(*xlrd.xldate_as_tuple((worksheet.cell(date_row,date_column)).value,workbook.datemode)).time()
            print (py_time)


               
            if (py_time <= threshold_time): #ensues that email is only sent when RFID is recorded before a certain threshold time
                
                target_email = ((worksheet.cell(date_row,email_column)).value)
                
                if (target_email != 42):
                    #print ("{0} is present to school today, attendance recorded at {1}".format((worksheet.cell(date_row,name_column).value),(py_time)) )
                    #print (date_row)
                    
                    #send email:
                    receiver_email = "{0}".format(target_email)
                    print (receiver_email) #identify email address
                    message = """\
Subject: Attendance

                
{0} is present to school today, attendance recorded at {1}""".format((worksheet.cell(date_row,name_column).value),(py_time))

                    send_email()
                

        

              

except:
   exit










