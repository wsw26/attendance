#include "MFRC522.h"
#include "SPI.h"
#define RST_PIN 15 // RST-PIN for RC522 - RFID - SPI - Modul GPIO5 
#define SS_PIN 4 // SDA-PIN for RC522 - RFID - SPI - Modul GPIO4 
MFRC522 mfrc522(SS_PIN, RST_PIN); // Create MFRC522 instance

int counter = 2;
char row[20];

void setup() {
  Serial.begin(9600);    // Initialize serial communications
  SPI.begin();          // Init SPI bus
  mfrc522.PCD_Init();    // Init MFRC522

  //Serial.println("CLEARSHEET");
  Serial.println("LABEL,Number ID");
}

void loop() { 
  // Look for new cards
  if ( ! mfrc522.PICC_IsNewCardPresent()) {
    delay(50);
    return;
  }
  // Select one of the cards
  if ( ! mfrc522.PICC_ReadCardSerial()) {
    delay(50);
    return;
  }
  // Show some details of the PICC (that is: the tag/card)
  //Serial.print(F("Card UID:"));
  //Serial.print("DATA,DATE,TIME," + F(dump_byte_array(mfrc522.uid.uidByte, mfrc522.uid.size)));//send the Name to excel
  //dump_byte_array(mfrc522.uid.uidByte, mfrc522.uid.size);
  //Serial.println();

  String card_uid = dump_byte_array(mfrc522.uid.uidByte, mfrc522.uid.size);

  //Serial.print("DATA,");
  //int number = counter;
  //String one = "CELL,SET,A";
  //String two = ", ";
  //String three = one + number + two ;
  //Serial.print(three);


  Serial.print("CELL,SET,A2,");
  Serial.println(card_uid);
  //counter = counter + 1;
  
  delay (1000);
  //Serial.println(card_uid);
  Serial.println("SAVEWORKBOOK");
 
}



// Helper routine to dump a byte array as hex values to Serial
//void dump_byte_array(byte *buffer, byte bufferSize) {
  //for (byte i = 0; i < bufferSize; i++) {
    //Serial.print(buffer[i] < 0x10 ? " 0" : " ");
    //Serial.print(buffer[i], HEX);
  //}
//}

String dump_byte_array(byte *buffer, byte bufferSize) {
  String id = "";
  for (byte i = 0; i < bufferSize; i++) {
    id += buffer[i] < 0x10 ? "0" : "";
    id += String(buffer[i], HEX);
  }
  return id;
}
